function coreController($scope, $element, $attrs, $http) {
//<INIT>  
    function initList() {
        $http.get('/listAllAction').
            then(function(success){
                if (success.data.length>0) {
                    $scope.coffeeList = success.data.sort(function(a,b){
                        return a.Position - b.Position;
                    });
                } else {
                    $scope.coffeeList = undefined;
                }
            }, function(error){
                    console.error(error);
            });
    }
//</INIT>
//<FUNCTIONS>
    $scope.delete = function(id){
      $http.delete('/removeAction/'+id).
            then(function(success){
                initList();
            }, function(error){
                    console.error(error);
            });
    };

    $scope.edit = function(element){
      if (element.newValue){
          $http.post('/editAction/'+element._id,element,{headers:{'Content-Type':'application/json'}}).
            then(function(success){
                initList();
            },function(error){
                console.error(error);
            });
      } else {
          alert('Please provide a new value for the update :)');
      }
    };

    $scope.add = function(){
        if ($scope.newEntry){
            var Position = 1;
            if($scope.coffeeList){
                Position = $scope.coffeeList[$scope.coffeeList.length-1].Position+1;
            }
            var data = {Name : $scope.newEntry, Position : Position};
            $http.post('/addAction',data,{headers:{'Content-Type':'application/json'}}).
                then(function(success){
                    $scope.newEntry = '';
                    initList();
                },function(error){
                    console.error(error);
                });
        } else {
            alert('Please provide a new value :)');
        }
    };

    $scope.move = function(id,direction){
        $http.put('/moveAction/'+id+'/'+direction).
            then(function(success){
                initList();
            }, function(error){
                    console.error(error);
            });
    };

    $scope.refresh = function() {
        initList();
    };
//</FUNCTIONS>
//<MAIN>
    initList();
//</MAIN>
}
//<DECLARATION>
app.component('core', {
  templateUrl: 'js/components/core.html',
  controller: coreController,
  bindings: { }
});
//</DECLARATION>
