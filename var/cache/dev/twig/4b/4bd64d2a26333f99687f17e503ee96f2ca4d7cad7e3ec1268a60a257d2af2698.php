<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_a2ce9604786291ff6be35f17c6c1228fb0754f19834e3de3dbf69fa8e65c81cf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_566d7b730d0bc6b2b9e98d515896d54b43da680f0f45d6d5a735d88ae4695e30 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_566d7b730d0bc6b2b9e98d515896d54b43da680f0f45d6d5a735d88ae4695e30->enter($__internal_566d7b730d0bc6b2b9e98d515896d54b43da680f0f45d6d5a735d88ae4695e30_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_566d7b730d0bc6b2b9e98d515896d54b43da680f0f45d6d5a735d88ae4695e30->leave($__internal_566d7b730d0bc6b2b9e98d515896d54b43da680f0f45d6d5a735d88ae4695e30_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_a4b9ae79a22996c41a2c88462b856222c1d29a09315ef4ecffb37125b7bb1a9f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a4b9ae79a22996c41a2c88462b856222c1d29a09315ef4ecffb37125b7bb1a9f->enter($__internal_a4b9ae79a22996c41a2c88462b856222c1d29a09315ef4ecffb37125b7bb1a9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpFoundationExtension')->generateAbsoluteUrl($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_a4b9ae79a22996c41a2c88462b856222c1d29a09315ef4ecffb37125b7bb1a9f->leave($__internal_a4b9ae79a22996c41a2c88462b856222c1d29a09315ef4ecffb37125b7bb1a9f_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_a0cf176fc083f5cac10cc7be5b3ff994a015ddf2fabd1d6a9dcb86a9fe4fe37b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a0cf176fc083f5cac10cc7be5b3ff994a015ddf2fabd1d6a9dcb86a9fe4fe37b->enter($__internal_a0cf176fc083f5cac10cc7be5b3ff994a015ddf2fabd1d6a9dcb86a9fe4fe37b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_a0cf176fc083f5cac10cc7be5b3ff994a015ddf2fabd1d6a9dcb86a9fe4fe37b->leave($__internal_a0cf176fc083f5cac10cc7be5b3ff994a015ddf2fabd1d6a9dcb86a9fe4fe37b_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_5afdfc162a0f74546e942c30386bc3f7de1a403128d463b55fee3946f204be98 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5afdfc162a0f74546e942c30386bc3f7de1a403128d463b55fee3946f204be98->enter($__internal_5afdfc162a0f74546e942c30386bc3f7de1a403128d463b55fee3946f204be98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_5afdfc162a0f74546e942c30386bc3f7de1a403128d463b55fee3946f204be98->leave($__internal_5afdfc162a0f74546e942c30386bc3f7de1a403128d463b55fee3946f204be98_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <link href=\"{{ absolute_url(asset('bundles/framework/css/exception.css')) }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
";
    }
}
