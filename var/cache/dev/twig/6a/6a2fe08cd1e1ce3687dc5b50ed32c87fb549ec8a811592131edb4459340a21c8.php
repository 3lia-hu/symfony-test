<?php

/* hello.html.twig */
class __TwigTemplate_7c6009b9a0b5b931fc3fa4bbe273e23dc9c9c2e4271e3def179582c714b81479 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "hello.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8bfb9b1d2017ef480c49f87321f722923a61481f80f856dd42797757bbb344d1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8bfb9b1d2017ef480c49f87321f722923a61481f80f856dd42797757bbb344d1->enter($__internal_8bfb9b1d2017ef480c49f87321f722923a61481f80f856dd42797757bbb344d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "hello.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8bfb9b1d2017ef480c49f87321f722923a61481f80f856dd42797757bbb344d1->leave($__internal_8bfb9b1d2017ef480c49f87321f722923a61481f80f856dd42797757bbb344d1_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_c391fb0db49f79a627705b2399e0166363433bf80f28a38e1749d76e5140be09 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c391fb0db49f79a627705b2399e0166363433bf80f28a38e1749d76e5140be09->enter($__internal_c391fb0db49f79a627705b2399e0166363433bf80f28a38e1749d76e5140be09_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "    ";
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : $this->getContext($context, "title")), "html", null, true);
        echo "
";
        
        $__internal_c391fb0db49f79a627705b2399e0166363433bf80f28a38e1749d76e5140be09->leave($__internal_c391fb0db49f79a627705b2399e0166363433bf80f28a38e1749d76e5140be09_prof);

    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        $__internal_99b8dee9bf2aaa3cfb919c5ed9f9f6326d0d8bb63d0fa61698be7a9318212d83 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_99b8dee9bf2aaa3cfb919c5ed9f9f6326d0d8bb63d0fa61698be7a9318212d83->enter($__internal_99b8dee9bf2aaa3cfb919c5ed9f9f6326d0d8bb63d0fa61698be7a9318212d83_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "    <h1>";
        echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : $this->getContext($context, "message")), "html", null, true);
        echo "</h1> ";
        echo twig_escape_filter($this->env, (isset($context["number"]) ? $context["number"] : $this->getContext($context, "number")), "html", null, true);
        echo "
";
        
        $__internal_99b8dee9bf2aaa3cfb919c5ed9f9f6326d0d8bb63d0fa61698be7a9318212d83->leave($__internal_99b8dee9bf2aaa3cfb919c5ed9f9f6326d0d8bb63d0fa61698be7a9318212d83_prof);

    }

    public function getTemplateName()
    {
        return "hello.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  51 => 7,  41 => 4,  35 => 3,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends \"base.html.twig\" %}

{% block title %}
    {{title}}
{% endblock %}

{% block body %}
    <h1>{{message}}</h1> {{number}}
{% endblock %}";
    }
}
