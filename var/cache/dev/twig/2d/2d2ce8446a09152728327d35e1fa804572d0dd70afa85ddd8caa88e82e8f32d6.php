<?php

/* base.html.twig */
class __TwigTemplate_2de7646b681ab602420cb79aa4f1fba70ecf38aceb3d7b0070b9478b859006b3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'javascript' => array($this, 'block_javascript'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_46e3c89b17336f08fd5dc2a114fc2046b55dcecfd972ee165caeac51de97583a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_46e3c89b17336f08fd5dc2a114fc2046b55dcecfd972ee165caeac51de97583a->enter($__internal_46e3c89b17336f08fd5dc2a114fc2046b55dcecfd972ee165caeac51de97583a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        
        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" /> 
        
        <script src=\"https://code.jquery.com/jquery-2.2.4.min.js\" integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\" crossorigin=\"anonymous\"></script>
        
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">
        <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script>
        ";
        // line 13
        $this->displayBlock('javascript', $context, $blocks);
        // line 14
        echo "        ";
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 15
        echo "    </head>
    <body>
        ";
        // line 17
        $this->displayBlock('body', $context, $blocks);
        // line 18
        echo "    </body>
</html>
";
        
        $__internal_46e3c89b17336f08fd5dc2a114fc2046b55dcecfd972ee165caeac51de97583a->leave($__internal_46e3c89b17336f08fd5dc2a114fc2046b55dcecfd972ee165caeac51de97583a_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_ca6b502cfc4b4cacad88e7c356bb9fa21349c46a1991e223258e5aac64c1d714 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ca6b502cfc4b4cacad88e7c356bb9fa21349c46a1991e223258e5aac64c1d714->enter($__internal_ca6b502cfc4b4cacad88e7c356bb9fa21349c46a1991e223258e5aac64c1d714_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_ca6b502cfc4b4cacad88e7c356bb9fa21349c46a1991e223258e5aac64c1d714->leave($__internal_ca6b502cfc4b4cacad88e7c356bb9fa21349c46a1991e223258e5aac64c1d714_prof);

    }

    // line 13
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_029ee51ae72e41d7274f6207d2a46408ad0b494dde2f35df647347a61768a67f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_029ee51ae72e41d7274f6207d2a46408ad0b494dde2f35df647347a61768a67f->enter($__internal_029ee51ae72e41d7274f6207d2a46408ad0b494dde2f35df647347a61768a67f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        echo " ";
        
        $__internal_029ee51ae72e41d7274f6207d2a46408ad0b494dde2f35df647347a61768a67f->leave($__internal_029ee51ae72e41d7274f6207d2a46408ad0b494dde2f35df647347a61768a67f_prof);

    }

    // line 14
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_3cec2ffe820f6f9b16d79beccba11608c90b0f138c9d0c58cc67ec811dd88bb4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3cec2ffe820f6f9b16d79beccba11608c90b0f138c9d0c58cc67ec811dd88bb4->enter($__internal_3cec2ffe820f6f9b16d79beccba11608c90b0f138c9d0c58cc67ec811dd88bb4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_3cec2ffe820f6f9b16d79beccba11608c90b0f138c9d0c58cc67ec811dd88bb4->leave($__internal_3cec2ffe820f6f9b16d79beccba11608c90b0f138c9d0c58cc67ec811dd88bb4_prof);

    }

    // line 17
    public function block_body($context, array $blocks = array())
    {
        $__internal_61c70731726fb4a003ad7e6f6de862e90d6e1cc7a9a631b5e51ad8946eec1e32 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_61c70731726fb4a003ad7e6f6de862e90d6e1cc7a9a631b5e51ad8946eec1e32->enter($__internal_61c70731726fb4a003ad7e6f6de862e90d6e1cc7a9a631b5e51ad8946eec1e32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_61c70731726fb4a003ad7e6f6de862e90d6e1cc7a9a631b5e51ad8946eec1e32->leave($__internal_61c70731726fb4a003ad7e6f6de862e90d6e1cc7a9a631b5e51ad8946eec1e32_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 17,  90 => 14,  78 => 13,  66 => 5,  57 => 18,  55 => 17,  51 => 15,  48 => 14,  46 => 13,  37 => 7,  32 => 5,  26 => 1,);
    }

    public function getSource()
    {
        return "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" /> 
        
        <script src=\"https://code.jquery.com/jquery-2.2.4.min.js\" integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\" crossorigin=\"anonymous\"></script>
        
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">
        <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script>
        {% block javascript %} {% endblock %}
        {% block stylesheets %}{% endblock %}
    </head>
    <body>
        {% block body %}{% endblock %}
    </body>
</html>
";
    }
}
