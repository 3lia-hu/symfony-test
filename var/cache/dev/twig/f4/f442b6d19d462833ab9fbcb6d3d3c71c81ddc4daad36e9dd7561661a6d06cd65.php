<?php

/* index.html.twig */
class __TwigTemplate_3ba0491fa231d8a980c33344f8ffc599c15854c5f7b3642c099210fbcb513103 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'javascript' => array($this, 'block_javascript'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9320f728e8ef1d3ee6384bd95c67da9029dcc6558816e470567d0924e43ff3a4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9320f728e8ef1d3ee6384bd95c67da9029dcc6558816e470567d0924e43ff3a4->enter($__internal_9320f728e8ef1d3ee6384bd95c67da9029dcc6558816e470567d0924e43ff3a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9320f728e8ef1d3ee6384bd95c67da9029dcc6558816e470567d0924e43ff3a4->leave($__internal_9320f728e8ef1d3ee6384bd95c67da9029dcc6558816e470567d0924e43ff3a4_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_1d8cad9863171296853e55b6f21dbbc08e4c455860695e32398072ed980aec05 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1d8cad9863171296853e55b6f21dbbc08e4c455860695e32398072ed980aec05->enter($__internal_1d8cad9863171296853e55b6f21dbbc08e4c455860695e32398072ed980aec05_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "    ";
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : $this->getContext($context, "title")), "html", null, true);
        echo "
";
        
        $__internal_1d8cad9863171296853e55b6f21dbbc08e4c455860695e32398072ed980aec05->leave($__internal_1d8cad9863171296853e55b6f21dbbc08e4c455860695e32398072ed980aec05_prof);

    }

    // line 7
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_b9b539e4795d187a69c3edcbadb7673470550c926f24f1e0fb2ec78d532b29e9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b9b539e4795d187a69c3edcbadb7673470550c926f24f1e0fb2ec78d532b29e9->enter($__internal_b9b539e4795d187a69c3edcbadb7673470550c926f24f1e0fb2ec78d532b29e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        // line 8
        echo "    <script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.8/angular.js\"></script>

    <script type=\"text/javascript\" src=\"js/index.js\"></script>
    <script type=\"text/javascript\" src=\"js/components/core.js\"></script>
";
        
        $__internal_b9b539e4795d187a69c3edcbadb7673470550c926f24f1e0fb2ec78d532b29e9->leave($__internal_b9b539e4795d187a69c3edcbadb7673470550c926f24f1e0fb2ec78d532b29e9_prof);

    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        $__internal_73589b958eaff03e80713261298e59f121cd2cf7c3edba4711f0c6ff7a5020ee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_73589b958eaff03e80713261298e59f121cd2cf7c3edba4711f0c6ff7a5020ee->enter($__internal_73589b958eaff03e80713261298e59f121cd2cf7c3edba4711f0c6ff7a5020ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 15
        echo "    <div class=\"container\" ng-app=\"Coffee\">
        <div class=\"row\">
            <div class=\"col-md-12 text-left\">
                <h1>";
        // line 18
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : $this->getContext($context, "title")), "html", null, true);
        echo " <small>list</small></h1>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12\">        
                <core></core>
            </div>
        </div>
    </div>
";
        
        $__internal_73589b958eaff03e80713261298e59f121cd2cf7c3edba4711f0c6ff7a5020ee->leave($__internal_73589b958eaff03e80713261298e59f121cd2cf7c3edba4711f0c6ff7a5020ee_prof);

    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 18,  75 => 15,  69 => 14,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends \"base.html.twig\" %}

{% block title %}
    {{title}}
{% endblock %}

{% block javascript %}
    <script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.8/angular.js\"></script>

    <script type=\"text/javascript\" src=\"js/index.js\"></script>
    <script type=\"text/javascript\" src=\"js/components/core.js\"></script>
{% endblock %}

{% block body %}
    <div class=\"container\" ng-app=\"Coffee\">
        <div class=\"row\">
            <div class=\"col-md-12 text-left\">
                <h1>{{title}} <small>list</small></h1>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12\">        
                <core></core>
            </div>
        </div>
    </div>
{% endblock %}";
    }
}
