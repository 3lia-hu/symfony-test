<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_c44a41d46e016de3697166766d7ecadd6658a1ce30507978d4bf73d3b82398da extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_762445e894e0e8542a4da2b4ad1d85236df1b63d32238a557220fa5fcd97e4c5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_762445e894e0e8542a4da2b4ad1d85236df1b63d32238a557220fa5fcd97e4c5->enter($__internal_762445e894e0e8542a4da2b4ad1d85236df1b63d32238a557220fa5fcd97e4c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_762445e894e0e8542a4da2b4ad1d85236df1b63d32238a557220fa5fcd97e4c5->leave($__internal_762445e894e0e8542a4da2b4ad1d85236df1b63d32238a557220fa5fcd97e4c5_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_c5df0cd1b58c086e3a9dc7921db1bc4671d8237c0c56ab82f2c1eec6ccb1b2b1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c5df0cd1b58c086e3a9dc7921db1bc4671d8237c0c56ab82f2c1eec6ccb1b2b1->enter($__internal_c5df0cd1b58c086e3a9dc7921db1bc4671d8237c0c56ab82f2c1eec6ccb1b2b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_c5df0cd1b58c086e3a9dc7921db1bc4671d8237c0c56ab82f2c1eec6ccb1b2b1->leave($__internal_c5df0cd1b58c086e3a9dc7921db1bc4671d8237c0c56ab82f2c1eec6ccb1b2b1_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_5c0cf743c8e529f915a02d1df56260512183dafab3d2a2dc5230c6a6d80a3171 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5c0cf743c8e529f915a02d1df56260512183dafab3d2a2dc5230c6a6d80a3171->enter($__internal_5c0cf743c8e529f915a02d1df56260512183dafab3d2a2dc5230c6a6d80a3171_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_5c0cf743c8e529f915a02d1df56260512183dafab3d2a2dc5230c6a6d80a3171->leave($__internal_5c0cf743c8e529f915a02d1df56260512183dafab3d2a2dc5230c6a6d80a3171_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_1b0c2c35bbf3ec09da48394a3abaa92709253d6da3181861b856c1fe4da9fc47 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1b0c2c35bbf3ec09da48394a3abaa92709253d6da3181861b856c1fe4da9fc47->enter($__internal_1b0c2c35bbf3ec09da48394a3abaa92709253d6da3181861b856c1fe4da9fc47_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_1b0c2c35bbf3ec09da48394a3abaa92709253d6da3181861b856c1fe4da9fc47->leave($__internal_1b0c2c35bbf3ec09da48394a3abaa92709253d6da3181861b856c1fe4da9fc47_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 33,  114 => 32,  108 => 28,  106 => 27,  102 => 25,  96 => 24,  88 => 21,  82 => 17,  80 => 16,  75 => 14,  70 => 13,  64 => 12,  54 => 9,  48 => 6,  45 => 5,  42 => 4,  36 => 3,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
";
    }
}
