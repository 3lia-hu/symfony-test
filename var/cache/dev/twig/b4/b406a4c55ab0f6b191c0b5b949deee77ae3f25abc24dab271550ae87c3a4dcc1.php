<?php

/* base.html */
class __TwigTemplate_95aa96d4418caddfb43517d6f75591fd2e6e5a973e6dfdf8ad6290f89c362384 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_de8cc52667e61b3a492ce30655a6aace8ded4f61c8ffd2c37df37067ae0e60ab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_de8cc52667e61b3a492ce30655a6aace8ded4f61c8ffd2c37df37067ae0e60ab->enter($__internal_de8cc52667e61b3a492ce30655a6aace8ded4f61c8ffd2c37df37067ae0e60ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        
        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" /> 
        
        <script src=\"https://code.jquery.com/jquery-2.2.4.min.js\" integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\" crossorigin=\"anonymous\"></script>
        
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\" integrity=\"sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp\" crossorigin=\"anonymous\">
        <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script>
    
        ";
        // line 15
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 16
        echo "    </head>
    <body>
        ";
        // line 18
        $this->displayBlock('body', $context, $blocks);
        // line 19
        echo "    </body>
</html>
";
        
        $__internal_de8cc52667e61b3a492ce30655a6aace8ded4f61c8ffd2c37df37067ae0e60ab->leave($__internal_de8cc52667e61b3a492ce30655a6aace8ded4f61c8ffd2c37df37067ae0e60ab_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_ddf92e693e45a6887fc9f5dbe5cebc806fb9957e74ee0ada9d674ffe6822346a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ddf92e693e45a6887fc9f5dbe5cebc806fb9957e74ee0ada9d674ffe6822346a->enter($__internal_ddf92e693e45a6887fc9f5dbe5cebc806fb9957e74ee0ada9d674ffe6822346a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_ddf92e693e45a6887fc9f5dbe5cebc806fb9957e74ee0ada9d674ffe6822346a->leave($__internal_ddf92e693e45a6887fc9f5dbe5cebc806fb9957e74ee0ada9d674ffe6822346a_prof);

    }

    // line 15
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_1cc42d54744b661856ba1daeec48c144a9e97a37830d15d9dd6312b66e07f3ee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1cc42d54744b661856ba1daeec48c144a9e97a37830d15d9dd6312b66e07f3ee->enter($__internal_1cc42d54744b661856ba1daeec48c144a9e97a37830d15d9dd6312b66e07f3ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_1cc42d54744b661856ba1daeec48c144a9e97a37830d15d9dd6312b66e07f3ee->leave($__internal_1cc42d54744b661856ba1daeec48c144a9e97a37830d15d9dd6312b66e07f3ee_prof);

    }

    // line 18
    public function block_body($context, array $blocks = array())
    {
        $__internal_3a7b8963c7f1012f2e1fdf9242e5bc9849c54a0bd5408e7cb99078c3ac2f6982 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3a7b8963c7f1012f2e1fdf9242e5bc9849c54a0bd5408e7cb99078c3ac2f6982->enter($__internal_3a7b8963c7f1012f2e1fdf9242e5bc9849c54a0bd5408e7cb99078c3ac2f6982_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_3a7b8963c7f1012f2e1fdf9242e5bc9849c54a0bd5408e7cb99078c3ac2f6982->leave($__internal_3a7b8963c7f1012f2e1fdf9242e5bc9849c54a0bd5408e7cb99078c3ac2f6982_prof);

    }

    public function getTemplateName()
    {
        return "base.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 18,  76 => 15,  64 => 5,  55 => 19,  53 => 18,  49 => 16,  47 => 15,  36 => 7,  31 => 5,  25 => 1,);
    }

    public function getSource()
    {
        return "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" /> 
        
        <script src=\"https://code.jquery.com/jquery-2.2.4.min.js\" integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\" crossorigin=\"anonymous\"></script>
        
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\" integrity=\"sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp\" crossorigin=\"anonymous\">
        <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script>
    
        {% block stylesheets %}{% endblock %}
    </head>
    <body>
        {% block body %}{% endblock %}
    </body>
</html>
";
    }
}
