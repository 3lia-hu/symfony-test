<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_b588d2bdbc47c72fba01484239510f7b4e23c3f71495f9b62ee4e5daa4f6b4f5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7814320d35177c1f954219865cafcb10b0fae985454aab7626c1d9dfa14143bd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7814320d35177c1f954219865cafcb10b0fae985454aab7626c1d9dfa14143bd->enter($__internal_7814320d35177c1f954219865cafcb10b0fae985454aab7626c1d9dfa14143bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7814320d35177c1f954219865cafcb10b0fae985454aab7626c1d9dfa14143bd->leave($__internal_7814320d35177c1f954219865cafcb10b0fae985454aab7626c1d9dfa14143bd_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_2b94e72b5e7930c16712496f92f6fa860727b032926c037671a83b8b688e999e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2b94e72b5e7930c16712496f92f6fa860727b032926c037671a83b8b688e999e->enter($__internal_2b94e72b5e7930c16712496f92f6fa860727b032926c037671a83b8b688e999e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_2b94e72b5e7930c16712496f92f6fa860727b032926c037671a83b8b688e999e->leave($__internal_2b94e72b5e7930c16712496f92f6fa860727b032926c037671a83b8b688e999e_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_0d3883e445d3993746bc1dd5bee08f4eb4e61bd993a0e7a67ca2f1da04dca93f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0d3883e445d3993746bc1dd5bee08f4eb4e61bd993a0e7a67ca2f1da04dca93f->enter($__internal_0d3883e445d3993746bc1dd5bee08f4eb4e61bd993a0e7a67ca2f1da04dca93f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_0d3883e445d3993746bc1dd5bee08f4eb4e61bd993a0e7a67ca2f1da04dca93f->leave($__internal_0d3883e445d3993746bc1dd5bee08f4eb4e61bd993a0e7a67ca2f1da04dca93f_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_ab6473cddd2bbaaab910a712e8151b60103bd90784f99e188fb52b819f3e2230 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ab6473cddd2bbaaab910a712e8151b60103bd90784f99e188fb52b819f3e2230->enter($__internal_ab6473cddd2bbaaab910a712e8151b60103bd90784f99e188fb52b819f3e2230_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_ab6473cddd2bbaaab910a712e8151b60103bd90784f99e188fb52b819f3e2230->leave($__internal_ab6473cddd2bbaaab910a712e8151b60103bd90784f99e188fb52b819f3e2230_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
";
    }
}
