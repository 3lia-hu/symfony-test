<?php

/* hello.html */
class __TwigTemplate_bbbbf0a9d04f6ccee5dbeb1766a2c557a5dc9bb72b5f480a71b656ef537f1549 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html", "hello.html", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_64f50edb93ca769812d461576c53b7886da1edcab96cd2a1c1e84bc1b7527d12 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_64f50edb93ca769812d461576c53b7886da1edcab96cd2a1c1e84bc1b7527d12->enter($__internal_64f50edb93ca769812d461576c53b7886da1edcab96cd2a1c1e84bc1b7527d12_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "hello.html"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_64f50edb93ca769812d461576c53b7886da1edcab96cd2a1c1e84bc1b7527d12->leave($__internal_64f50edb93ca769812d461576c53b7886da1edcab96cd2a1c1e84bc1b7527d12_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_131fb835af680a0bd687cc25323179ce45aba9ac4fa213e9bc3546494f098a47 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_131fb835af680a0bd687cc25323179ce45aba9ac4fa213e9bc3546494f098a47->enter($__internal_131fb835af680a0bd687cc25323179ce45aba9ac4fa213e9bc3546494f098a47_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "    ";
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : $this->getContext($context, "title")), "html", null, true);
        echo "
";
        
        $__internal_131fb835af680a0bd687cc25323179ce45aba9ac4fa213e9bc3546494f098a47->leave($__internal_131fb835af680a0bd687cc25323179ce45aba9ac4fa213e9bc3546494f098a47_prof);

    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        $__internal_1c20760d386c1d7027d0fd6b6c26f4ef9a5c525105b8b378b8b8f009b893d5fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1c20760d386c1d7027d0fd6b6c26f4ef9a5c525105b8b378b8b8f009b893d5fd->enter($__internal_1c20760d386c1d7027d0fd6b6c26f4ef9a5c525105b8b378b8b8f009b893d5fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "    <h1>";
        echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : $this->getContext($context, "message")), "html", null, true);
        echo "</h1> ";
        echo twig_escape_filter($this->env, (isset($context["number"]) ? $context["number"] : $this->getContext($context, "number")), "html", null, true);
        echo "
";
        
        $__internal_1c20760d386c1d7027d0fd6b6c26f4ef9a5c525105b8b378b8b8f009b893d5fd->leave($__internal_1c20760d386c1d7027d0fd6b6c26f4ef9a5c525105b8b378b8b8f009b893d5fd_prof);

    }

    public function getTemplateName()
    {
        return "hello.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  51 => 7,  41 => 4,  35 => 3,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends \"base.html\" %}

{% block title %}
    {{title}}
{% endblock %}

{% block body %}
    <h1>{{message}}</h1> {{number}}
{% endblock %}";
    }
}
