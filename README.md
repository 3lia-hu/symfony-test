Symfony Test
============

## Prerequisities ##
* PHP5;
* MongoDB 3.2;
* PHP driver for MongoDB installed and enabled;

## Installation ##
Clone the repository locally:

```
#!bash

git clone https://3lia-hu@bitbucket.org/3lia-hu/symfony-test.git
```
## Launch ##
Move into the folder created:

```
#!bash

cd symfony-test
```
Launch the app:

```
#!bash

php bin/console server:run
```
You can reach the app from by pointing your browser to the URL:
[http://127.0.0.1:8000/app](http://127.0.0.1:8000/app)

Or otherwise displayed by the run command, plus '/app'.