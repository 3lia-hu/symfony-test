<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use EB\CoffeeBundle\Document\Coffee;

class CoffeeController extends Controller
{
    /**
     * @Route("/listAllAction")
     * @Method({"GET"})
     */
    public function listAllAction()
    {
        $repository = $this->get('doctrine_mongodb')
            ->getManager()
            ->getRepository('EBCoffeeBundle:Coffee');
        $list = $repository->findAll();

        $res = array();
        foreach ($list as $value) {
            array_push($res,Array(
                            'Name'=>$value->getName(),
                            'Position'=>$value->getPosition(),
                            '_id'=>$value->getId(),
            ));
        }

        return new Response(json_encode($res));
    }

    /**
     * @Route("/editAction/{id}")
     * @Method({"POST"})
     */
    public function editAction(Request $request, $id)
    {
        $data = [];
        if ($content = $request->getContent()) {
            $data = json_decode($content, true);
        }

        $em = $this->get('doctrine_mongodb')->getManager();
        $element = $em->getRepository('EBCoffeeBundle:Coffee')->find($id);

        if (!$element) {
            throw $this->createNotFoundException(
                'No coffee found for id '.$id
            );
        }

        $element->setName($data['newValue']);
        $em->flush();

        return new Response(json_encode('{"message":"Success"}'));
    }

    /**
     * @Route("/addAction")
     * @Method({"POST"})
     */
    public function addAction(Request $request)
    {
        
        $data = [];
        if ($content = $request->getContent()) {
            $data = json_decode($content, true);
        }

        $coffee = new Coffee();
        $coffee->setName($data['Name']);
        $coffee->setPosition($data['Position']);

        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($coffee);
        $dm->flush();

        return new Response(json_encode('{"message":"Success"}'));
    }

    /**
     * @Route("/removeAction/{id}")
     * @Method({"DELETE"})
     */
    public function removeAction($id)
    {
        $em = $this->get('doctrine_mongodb')->getManager();
        $coffee = $em->getRepository('EBCoffeeBundle:Coffee')->find($id);
        $em->remove($coffee);
        $em->flush();
        return new Response(json_encode('{"message":"Success"}'));
    }

    /**
     * @Route("/moveAction/{id}/{direction}")
     * @Method({"PUT"})
     */
    public function moveAction($id,$direction)
    {
        
        settype($direction, "integer");

        $em = $this->get('doctrine_mongodb')->getManager();
        $coffee = $em->getRepository('EBCoffeeBundle:Coffee')->find($id);
        
        $moveTo = $coffee->getPosition()+$direction;
        $otherCoffee = $em->getRepository('EBCoffeeBundle:Coffee')->findOneByPosition($moveTo);

        if ($otherCoffee) {
            $otherCoffee->setPosition($coffee->getPosition());
        }

        $coffee->setPosition($moveTo);

        $em->flush();

        return new Response(json_encode('{"message":"Success"}'));
    }
}
